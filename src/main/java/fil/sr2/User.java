package fil.sr2;

/**
 * Classe representant un utilisateur de la plateforme FlopBox
 * 
 * @author Bouali Hocine
 *
 */
public class User {
	private String username;
	private String password;

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return getUsername();
	}

	/**
	 * l'égalité est testée sur le nom uniquement
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User u = (User) obj;

		return u.getUsername().trim().equals(getUsername().trim());
	}

}
