package fil.sr2.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPSClient;

/**
 * Classe qui contient des methodes permettant la bonne implementation de l'API
 * FlopBox
 * 
 * @author Bouali Hocine
 *
 */
public class Utils {

	/**
	 * Méthode qui permet de lister et d'afficher les informations des fichiers
	 * passés en paramètre
	 * 
	 * @param files une liste de fichiers/dossiers
	 * @return renvoie une chaine contenant les informations à afficher
	 */
	public static String fileDetails(FTPFile[] files) {
		DateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String res = "";
		for (FTPFile file : files) {
			String details = file.getName();

			if (file.isDirectory()) {
				details = "[" + details + "]";
			}
			
//			else {
//				details = "<" + details + ">";
//			}
			
			else if(file.isFile()){
				details = "<" + details + ">";
			}
			
			else {
				details = "/"+details+"/";
			}
			details += "    " + file.getSize();
			details += "    " + file.getUser();
			details += "    " + dateFormater.format(file.getTimestamp().getTime());
			res += details + "\n";

		}
		return res;
	}

	/**
	 * Méthode qui permet de télécharger un dossier distant
	 * 
	 * @param ftp        un client FTP préalablement initialisé
	 * @param remotePath le chemin d'accès du dossier distant
	 * @param localPath  le chemin d'accès local
	 * @throws IOException probleme lors du téléchargement d'un fichier
	 */
	public static void download(FTPClient ftp, String remotePath, String localPath) throws IOException {

		File basedir = new File(localPath);
		basedir.mkdirs();
		FTPFile[] remoteFiles;
		remoteFiles = ftp.listFiles(remotePath);

		for (FTPFile remoteFile : remoteFiles) {
			if (!remoteFile.getName().equals(".") && !remoteFile.getName().equals("..")) {
				String remoteFilePath = remotePath + "/" + remoteFile.getName();
				String localFilePath = localPath + "/" + remoteFile.getName();
				if (remoteFile.isDirectory()) {
					new File(localFilePath).mkdirs();
					download(ftp, remoteFilePath, localFilePath);
				} else {

					OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(localFilePath));

					if (!ftp.retrieveFile(remoteFilePath, outputStream)) {
						throw new IOException();
					}
					outputStream.close();
				}
			}
		}

	}

	public static void zip(FTPClient ftp, String folder, ZipOutputStream zos)
			throws FileNotFoundException, IOException {
		FTPFile[] remoteFiles = ftp.listFiles(folder);
		for (FTPFile remoteFile : remoteFiles) {
			String remoteFilePath = folder + "/" + remoteFile.getName();
			if (remoteFile.isDirectory()) {
				zip(ftp, remoteFilePath, zos);
				continue;
			}
			zos.putNextEntry(new ZipEntry(remoteFilePath));
			BufferedInputStream bis = new BufferedInputStream(ftp.retrieveFileStream(remoteFilePath));
			zos.write(bis.readAllBytes());
			zos.closeEntry();
		}
	}

	/**
	 * Méthode qui permet de supprimer un repertoire distant
	 * 
	 * @param ftp        un client FTP préalablement initialisé
	 * @param remotePath le chemin d'accès du dossier à supprimer
	 * @throws IOException si l'un des fichiers n'a pas pu être supprimé
	 */
	public static void removedir(FTPClient ftp, String remotePath) throws IOException {
		FTPFile[] remoteFiles = ftp.listFiles(remotePath);
		for (FTPFile remoteFile : remoteFiles) {
			if (!remoteFile.getName().equals(".") && !remoteFile.getName().equals("..")) {
				String remoteFilePath = remotePath + "/" + remoteFile.getName();

				if (remoteFile.isDirectory()) {
					removedir(ftp, remoteFilePath);
				} else {
					if (!ftp.deleteFile((remoteFilePath))) {
						throw new IOException();
					}
				}
			}
		}
		ftp.sendCommand("RMD " + remotePath);
	}

	/**
	 * Méthode qui permet dinitialiser une connexion à un serveur FTP
	 * 
	 * @param server   l'adresse du serveur FTP
	 * @param username le username
	 * @param psw      le mot de passe
	 * @param port     le port de connexion
	 * @param mode     le mode d'echange avec le serveur (PASV ou PORT) par defaut
	 *                 en PASV
	 * @return un serveur FTP initialisé
	 * @throws SocketException un probleme interne est survenu lors de la connexion
	 *                         au serveur
	 * @throws IOException     l'utilisateur a mal entrer ses données
	 */
	public static FTPClient initftp(String server, String username, String psw, int port, String mode)
			throws SocketException, IOException {
		FTPClient f = new FTPClient();
		f.connect(server, port);
		f.login(username, psw);
		if (mode.equals("pasv")) {
			f.enterLocalPassiveMode();
		} else if (!mode.equals("port")) {
			throw new IOException();
		}
		return f;
	}

	/**
	 * Méthode qui permet d'initialiser une connexion sécurisée à un serveur FTPS
	 * 
	 * @param server   l'adresse du serveur FTPS
	 * @param username le username
	 * @param psw      le mot de passe
	 * @param port     le port de connexion
	 * @param mode     le mode d'echange avec le serveur (PASV ou PORT) par defaut
	 *                 en PASV
	 * @return un serveur FTPS initialisé
	 * @throws SocketException un probleme interne est survenu lors de la connexion
	 *                         au serveur
	 * @throws IOException     l'utilisateur a mal entrer ses données
	 */
	public static FTPSClient initftps(String server, String username, String psw, int port, String mode)
			throws SocketException, IOException {
		FTPSClient f = new FTPSClient("SSL");
		f.connect(server, port);
		f.login(username, psw);
		f.sendCommand("PBSZ 0");
		f.sendCommand("PROT P");
		f.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
		if (mode.equals("pasv")) {
			f.enterLocalPassiveMode();
		} else if (!mode.equals("port")) {
			throw new IOException();
		}
		return f;
	}

	/**
	 * Méthode qui permet de chercher un alias dans la map et d'en renvoyer la
	 * valeur
	 * 
	 * @param alias un alias
	 * @param map   une map
	 * @return l'adresse associée à l'alias
	 * @throws NullPointerException si l'alias n'existe pas
	 */
	public static String findAddress(String alias, Map<String, String> map) throws NullPointerException {
		if (!map.containsKey(alias)) {
			throw new NullPointerException();
		}
		return map.get(alias);
	}

	/**
	 * Méthode qui permet de parser le nom d'un fichier
	 * 
	 * @param filename le nom du fichier
	 * @return un tableau de taille 2 qui contient en première position le nom et en
	 *         deuxième position l'extension
	 */
	public static String[] parse(String filename) {
		String[] res = new String[2];

		res[0] = filename.substring(0, filename.indexOf("."));
		res[1] = filename.substring(filename.indexOf("."), filename.length());
		return res;
	}

	/**
	 * Méthode qui permet d'upload un fichier sur un serveur FTP
	 * 
	 * @param ftp  un client FTP préalablement initialisé
	 * @param path le chemin d'acces du repertoire parent sur le serveur
	 * @return renvoie true si le fichier a été upload et false sinon
	 * @throws IOException probleme lors de l'ecriture dans le coté serveur
	 */
	public static boolean uploadFile(FTPClient ftp, String path, InputStream in) throws IOException {

		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		ftp.enterLocalPassiveMode();
		return ftp.storeFile(path, in);

	}
}
