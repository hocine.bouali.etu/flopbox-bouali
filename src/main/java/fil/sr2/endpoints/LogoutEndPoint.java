package fil.sr2.endpoints;

import java.io.IOException;
import java.util.Collections;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fil.sr2.Main;
import fil.sr2.Secured;

@Path("logout")
public class LogoutEndPoint {

	/**
	 * Méthode qui permet de se deconnecter de la plateforme FlopBox ( supprimer le
	 * token d'authentification)
	 * 
	 * @param username un username
	 * @return un statut http
	 */
	@POST
	@Secured
	@Produces(MediaType.TEXT_PLAIN)
	public Response logout(@QueryParam("username") String username) {
		try {
			if (!findToken(username)) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("try later pls !").build();
			}
		} catch (IOException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(username + " not found").build();
		}
		return Response.ok("logout successful").build();
	}

	private boolean findToken(String username) throws IOException {
		if (Main.token.containsValue(username)) {
			throw new IOException();
		}
		return Main.token.values().removeAll(Collections.singleton(username));
	}

}
