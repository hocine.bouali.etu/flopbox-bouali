package fil.sr2.endpoints;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fil.sr2.Main;
import fil.sr2.Secured;

/**
 * Cette classe représente la ressource "/serveur", cette ressource permet de
 * créer des associations (alias,adresse), de les lister, de les modifier et de
 * les supprimer
 * 
 * @author Bouali Hocine
 *
 */
@Path("serveur")
public class ServerEndPoint {

	/**
	 * Méthode qui permet de lister toutes les associations déjà enregistrées
	 * 
	 * @return statut 200
	 */
	@GET
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAllias() {
		return Response.status(Response.Status.OK).entity(Main.allAlias).type(MediaType.APPLICATION_JSON).build();
	}

	/**
	 * Méthode qui permet de créer une association
	 * 
	 * @param alias  un alias
	 * @param server une adresse
	 * @return statut 200 si tout s'est bien passer ,403 si l'alias existe déjà
	 */
	@POST
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response newAlias(@QueryParam("alias") String alias, @QueryParam("serveur") String server) {
		if (Main.allAlias.containsKey(alias)) {
			return Response.status(Response.Status.FORBIDDEN).entity("already exist").build();
		}
		Main.allAlias.put(alias, server);
		String message = "{\"alias\": \"" + alias + "\",\"serveur\":\"" + server + "\"}";
		return Response.status(Response.Status.OK).entity(message).type(MediaType.APPLICATION_JSON).build();
	}

	/**
	 * Méthode qui permet de mettre à jour l'alias
	 * 
	 * @param oldAlias l'ancien alias
	 * @param newAlias le nouvel alias
	 * @return statut 200 si la mise à jour s'est bien passé, 404 si l'alias
	 *         n'existe pas, 403 si le nouvel alias existe déjà
	 */
	@PUT
	@Secured
	@Path("/alias")
	@Produces(MediaType.TEXT_PLAIN)
	public Response updateAlias(@QueryParam("oldalias") String oldAlias, @QueryParam("newalias") String newAlias) {

		if (!Main.allAlias.containsKey(oldAlias)) {
			return Response.status(Response.Status.NOT_FOUND).entity(oldAlias + " not exist").build();
		}
		if (Main.allAlias.containsKey(newAlias)) {
			return Response.status(Response.Status.FORBIDDEN).entity(newAlias + " already exist").build();
		}
		String tmp = Main.allAlias.get(oldAlias);
		Main.allAlias.put(newAlias, tmp);
		Main.allAlias.remove(oldAlias);
		return Response.status(Response.Status.OK).entity(newAlias + " is the new alias for " + tmp).build();
	}

	/**
	 * Méthode qui permet de mettre à jour l'adresse
	 * 
	 * @param alias  l'alias de l'adresse à modifier
	 * @param server la nouvelle adresse
	 * @return statut 200 si la mise à jour s'est bien passé, 404 si l'alias
	 *         n'existe pas
	 */
	@PUT
	@Secured
	@Path("server")
	@Produces(MediaType.TEXT_PLAIN)
	public Response updateServer(@QueryParam("alias") String alias, @QueryParam("new-address") String server) {
		if (!Main.allAlias.containsKey(alias)) {
			return Response.status(Response.Status.NOT_FOUND).entity(alias + " not exist").build();
		}
		Main.allAlias.put(alias, server);
		return Response.status(Response.Status.OK).entity(server + " is the new address for " + alias).build();
	}

	/**
	 * Méthode qui permet de supprimer une association (alias, adresse)
	 * 
	 * @param alias l'alias à supprimer
	 * @return statut 404 si l'alias n'existe pas et statut 200 si tout c'est bien
	 *         passer
	 */
	@DELETE
	@Secured
	@Produces(MediaType.TEXT_PLAIN)
	public Response removeAssociation(@QueryParam("alias") String alias) {
		if (!Main.allAlias.containsKey(alias)) {
			return Response.status(Response.Status.NOT_FOUND).entity("Alias not exist").build();
		}
		Main.allAlias.remove(alias);
		return Response.status(Response.Status.OK).entity("alias " + alias + " removed").build();
	}
}
