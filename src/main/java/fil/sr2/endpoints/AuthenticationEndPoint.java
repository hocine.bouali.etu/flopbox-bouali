package fil.sr2.endpoints;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fil.sr2.Main;
import fil.sr2.User;

/**
 * Cette classe représente la ressource "/login", cette ressource permet de se
 * connecter sur la plateforme FlopBox
 * 
 * @author Bouali Hocine
 *
 */
@Path("login")
public class AuthenticationEndPoint {

	/**
	 * Méthode qui permet d'authentifier un utilisateur et
	 * 
	 * @param username un username
	 * @param password un mot de passe
	 * @return renvoie le token généré aléatoirement
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response authenticateUser(@FormParam("username") String username, @FormParam("password") String password) {

		try {
			authenticate(username, password);

			String token = issueToken(username);

			return Response.ok(token).build();

		} catch (Exception e) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
	}

	private void authenticate(String username, String password) throws Exception {
		User user = new User(username, password);
		// Verifie si notre utilisateur existe
		if (!(Main.users.contains(user)))
			throw new Exception();
		// etant donner que l'utilisateur existe , on verifie donc le mot de passe
		if (!(Main.users.get(Main.users.indexOf(user)).getPassword().equals(password)))
			throw new Exception();
	}

	private String issueToken(String username) {
		// Genere un token aleatoirement
		Random random = new SecureRandom();
		String token = new BigInteger(130, random).toString(32);
		Main.token.put(token, username);
		return token;
	}
}
