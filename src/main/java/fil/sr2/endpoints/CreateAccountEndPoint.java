package fil.sr2.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fil.sr2.Main;
import fil.sr2.User;

/**
 * Cette classe représente la ressource "/createAcc", cette ressource permet de
 * creer un compte sur la plateforme FlopBox
 * 
 * @author Bouali Hocine
 *
 */
@Path("createAcc")
public class CreateAccountEndPoint {

	/**
	 * Méthode qui permet de à un utilisateur de creer un compte FlopBox
	 * 
	 * @param username son nom
	 * @param password son mot de passe
	 * @return un statut http
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createAccount(@FormParam("username") String username, @FormParam("password") String password) {
		User newUser = new User(username, password);
		if (Main.users.contains(newUser)) {
			return Response.status(Response.Status.FORBIDDEN).entity("already exist").build();
		}

		Main.users.add(newUser);
		String message = "{\"username\": \"" + newUser.getUsername() + "\"}";
		return Response.status(Response.Status.OK).entity(message).type(MediaType.APPLICATION_JSON).build();
	}
}
