## Implementation d'un serveur FTP

 Bouali Hocine    
  		
 30/03/2021

#### Introduction

Ce projet permet d'interagir avec un serveur FTP grace à une API REST. 

Il est possible de télécharger, d'uploader, de supprimer et de renommer des fichiers et des dossiers  ainsi que de créer des répertoires.

Il est également possible de lister le contenu d'un répertoire 

Pour cela , il est nécessaire d'être connecté et donc d'avoir un compte FlopBox, l'authentification à la plateforme utilise la *Bearer authentification* et donc permet de maintenir une session ouverte grâce à des Tokens


#### Exigences

L'ensemble des exigences ont été respectées.
Pour l'envoie et le téléchargement de dossier,il est necessaire des les avoir zipper au préalable 

## Utilisation 

##### Etape 1: récupérer le projet

 Clone du dépôt dans le répertoire de votre choix

	$git clone https://gitlab.univ-lille.fr/hocine.bouali.etu/flopbox-bouali.git
 
 Placez-vous dans le bon dossier

	$cd flopbox-bouali/

##### Etape 2: exécuter le projet

 Compilation du projet 
 
	$mvn clean compile
 
 Exécution du projet:
 
	$mvn exec:java



### Création d'un compte

	Parametres: <username> et <password> de type x-www-form-urlencoded
	
	POST http://127.0.0.1:8080/myapp/createAcc
	
### Authentification

Si l'authentification réussie, un token est renvoyé
	
	Parametres: <username> et <password> de type x-www-form-urlencoded
	
	POST http://127.0.0.1:8080/myapp/login


### Gestion des Alias

Affichage des associations

	GET http://127.0.0.1:8080/myapp/serveur 

Creation d'une association

	POST http://127.0.0.1:8080/myapp/serveur?alias=votrealias&serveur=votreserveur
	
Modification d'un alias
	
	POST http://127.0.0.1:8080/myapp/serveur/alias?oldalias=alias&newalias=nouvelalias
	
Modification d'une adresse

	POST http://127.0.0.1:8080/myapp/serveur/server?alias=unalias&new-address=nouvelleadresse
	
	
Suppression d'une association

	DELETE http://127.0.0.1:8080/myapp/serveur?alias=unalias




Le Header de chacune des requetes suivantes devra contenir :


Parametre              | Valeur         | Obligatoire
-----------------------|----------------| -----------------
Authorization: Bearer  | token          | oui
username               | username FTP   | non (par defaut anonymous)
password               | password FTP   | non (par defaut anonymous)
type                   | FTP / FTPS     | non (par defaut FTP)
mode                   | PASV/PORT      | non (par defaut PASV)
port                   | port du serveur| non (par defaut 21)


### Echange avec un serveur FTP

lister un reperoire
	
	GET http://127.0.0.1:8080/myapp/ftp/{alias}/list/{path}

Télécharger un fichier/dossier zippé
	
	GET http://127.0.0.1:8080/myapp/ftp/{alias}/file/{path}

	
Supprimer un fichier
	
	DELETE http://127.0.0.1:8080/myapp/ftp/{alias}/file/{path}

Supprimer un dossier 

	DELETE http://127.0.0.1:8080/myapp/ftp/{alias}/dir/{path}
	
upload un fichier/dossier zippé

	Form-data param le fichier à envoyer 

	POST http://127.0.0.1:8080/myapp/ftp/{alias}/file/{path}

	
renommer un fichier ou  dossier
	
	
	PUT http://127.0.0.1:8080/myapp/ftp/{alias}/{path}?name=newname
	
Créer un dossier
	
	POST http://127.0.0.1:8080/myapp/ftp/{alias}/new-dir/{dest}?name=nomdudossier
	

	
Une vidéo de démonstration est disponible dans *doc/demo.mkv*

les parties sur l'upload et download de dossier ne sont plus valide dans la vidéo, il est necessaire de zipper et de les envoyer en tant que fichier

Le diagramme UML est également disponible dans *doc/uml.png*

# Architecture



##### Gestion d'erreur


Lorsqu'une exception est levée, le code d'erreur correspondant est renvoyé  

	try {
		...

		return Response.ok(token).build();

	} catch (Exception e) {
		return Response.status(Response.Status.FORBIDDEN).build();
	}

Exemple 2
	
	private Response init(String alias, String type, String username, String psw, String mode, int port) {
		try {
			...
		} catch (SocketException e1) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("try later pls !").build();
		} catch (IOException e2) {
			return Response.status(Response.Status.FORBIDDEN).entity("impossible to connect to " + server).build();
		}
		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	
	
# Code Samples


Méthode qui permet de télécharger un fichier dans un répertoire choisi par le client, si le fichier existe déjà , il n'écrase pas l'ancien mais va modifier le nom en incrémentant un compteur, l'annotation **@Secured** permet d'utiliser cette fonction uniquement si l'utilisateur est authentifié

	@GET
	@Secured
	@Path("{alias}/file/{path: .*}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadHttpFile(...) {

		Response r = init(alias, type, username, psw, mode, port);
		if (!(r.getStatus() == Response.Status.ACCEPTED.getStatusCode())) {// on verifie que l'init s'est bien passer
			return r;
		}
		String filename = path.substring(path.lastIndexOf("/") + 1, path.length());
		File file = new File("./Downloads/" + filename);
		OutputStream output;
		try {
			output = new FileOutputStream(file);
			if (!ftp.retrieveFile(path, output)) {
				output.close();
				throw new IOException();
			}
			output.close();
		} catch (IOException e) {
			...
		}
		return Response.ok(file).header("Content-Disposition", "attachment; filename=\"" + filename + "\"").build();
	}



Méthode qui permet d'initialiser une connexion à un serveur FTP et de choisir le port et le mode de connexion(passif ou actif)

	private Response init(String alias, String type, String username, String psw, String mode, int port) {
		...
		try {
			switch (type.toLowerCase()) {
			case "ftp":
				ftp = Utils.initftp(server, username, psw, 21, mode);
				break;
			case "ftps":
				ftp = Utils.initftps(server, username, psw, 21, mode);
				break;
			default:
				return Response.status(Response.Status.FORBIDDEN).entity(type + " not supported or not exist").build();
			}
		} catch (SocketException e1) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("try later pls !").build();
		} catch (IOException e2) {
			return Response.status(Response.Status.FORBIDDEN).entity("impossible to connect to " + server).build();
		}
		return Response.status(Response.Status.ACCEPTED).build();
	}
